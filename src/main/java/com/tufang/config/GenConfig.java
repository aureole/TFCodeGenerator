package com.tufang.config;

/**
 * 代码生成配置
 * @author: KevinOy
 * @date: 2019/03/17 13:06
 */
public class GenConfig {
    /**作者*/
    private String author;
    /**包名*/
    private String packageName;
    /**模块名*/
    private String moduleName;
    /**输出路径*/
    private String outputPath;
    /**数据库类型: oracle, mysql, sqlserver*/
    private String dbType;
    /**数据库URL*/
    private String url;
    /**用户名*/
    private String username;
    /**密码*/
    private String password;
    /**表名*/
    private String tableName;
    /**是否将数据库中下划线转驼峰*/
    private boolean isUnderLine2Camel = true;
    /**前缀*/
    private String tablePrefix;
    /**模板目录*/
    private String templatePath;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public boolean isUnderLine2Camel() {
        return isUnderLine2Camel;
    }

    public void setUnderLine2Camel(boolean underLine2Camel) {
        isUnderLine2Camel = underLine2Camel;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getTablePrefix() {
        return tablePrefix;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }
}
