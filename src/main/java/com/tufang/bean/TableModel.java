package com.tufang.bean;

import java.util.List;
import java.util.Map;

/**
 * 表Model类，对应一张表
 * @author: KevinOy
 * @date: 2019/03/18 23:19
 */
public class TableModel {
    private String packageName;//java entity和mapper所在包名
    private String className;//类名
    private String tableName;//表名
    private List<DbColumn> columnList;//列信息
    private Map<String, String> importPackageMap;//导包信息，便于模板展示

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<DbColumn> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<DbColumn> columnList) {
        this.columnList = columnList;
    }

    public Map<String, String> getImportPackageMap() {
        return importPackageMap;
    }

    public void setImportPackageMap(Map<String, String> importPackageMap) {
        this.importPackageMap = importPackageMap;
    }
}
