package com.tufang.bean;


/**
 * 数据库列信息
 * @author: KevinOy
 * @date: 2019/03/17 13:32
 */
public class DbColumn {
    /**字段名称*/
    private String columnDbName;
    /**字段名称(首字母小写)*/
    private String columnJavaName;
    /**字段名称(首字母大写)*/
    private String columnJavaNameFirstUpper;
    /**数据库类型*/
    private String columnDbType;
    /**Java类型*/
    private String columnJavaType;
    /**Java导包路径*/
    private String importPackagePath;
    /**注释*/
    private String comment;

    public String getColumnDbName() {
        return columnDbName;
    }

    public void setColumnDbName(String columnDbName) {
        this.columnDbName = columnDbName;
    }

    public String getColumnJavaName() {
        return columnJavaName;
    }

    public void setColumnJavaName(String columnJavaName) {
        this.columnJavaName = columnJavaName;
    }

    public String getColumnDbType() {
        return columnDbType;
    }

    public void setColumnDbType(String columnDbType) {
        this.columnDbType = columnDbType;
    }

    public String getColumnJavaType() {
        return columnJavaType;
    }

    public void setColumnJavaType(String columnJavaType) {
        this.columnJavaType = columnJavaType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImportPackagePath() {
        return importPackagePath;
    }

    public void setImportPackagePath(String importPackagePath) {
        this.importPackagePath = importPackagePath;
    }

    public String getColumnJavaNameFirstUpper() {
        return columnJavaNameFirstUpper;
    }

    public void setColumnJavaNameFirstUpper(String columnJavaNameFirstUpper) {
        this.columnJavaNameFirstUpper = columnJavaNameFirstUpper;
    }
}
