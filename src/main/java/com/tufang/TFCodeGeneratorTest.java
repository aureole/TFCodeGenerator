package com.tufang;

import com.tufang.config.GenConfig;
import com.tufang.core.CodeGenerator;

/**
 * main主类
 * @author: KevinOy
 * @date: 2019/03/17 12:48
 */
public class TFCodeGeneratorTest {
    public static void main(String[] args) throws Exception {
        GenConfig config = new GenConfig();
        config.setTemplatePath("/TFCodeTemplate");//模板路径
        config.setPackageName("com.tufang.modules.sys.user");//包名
        config.setModuleName("sys.user");//模块名，用于生成mapper xml文件分包
        config.setOutputPath("D:\\CodeTemplates");//输出目录
        config.setTablePrefix("tb");//表名前缀
        config.setAuthor("KevinOy");//作者
        config.setDbType("mysql");//数据库类型，暂时只支持MySQL
        config.setUrl("jdbc:mysql://localhost:3306/tufang?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT%2B8");
        config.setUsername("root");
        config.setPassword("123456");
        config.setTableName("tb_sys_user");//生成表名，多个以,分隔
        //config.setTableName("syslog");
        CodeGenerator c = CodeGenerator.GetInstance(config);
        c.generate();
    }
}
