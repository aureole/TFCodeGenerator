package com.tufang.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * 数据库util
 * @author: KevinOy
 * @date: 2019/03/18 22:59
 */
public class DbUtils {

    public static Connection getConnection(String url, String username, String password) {
        try {
            //连接信息
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            System.out.println("数据库连接获取错误");
            e.printStackTrace();
        }
        return null;
    }

    public static void close(PreparedStatement ps, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception e) {
                //ignore
            }
        }
        if (ps != null) {
            try {
                ps.close();
            } catch (Exception e) {
                //ignore
            }
        }
    }

    public static void close(Connection c, PreparedStatement ps, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception e) {
                //ignore
            }
        }
        if (ps != null) {
            try {
                ps.close();
            } catch (Exception e) {
                //ignore
            }
        }
        if (c != null) {
            try {
                c.close();
            } catch (Exception e) {
                //ignore
            }
        }
    }
}
