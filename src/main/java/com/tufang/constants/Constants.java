package com.tufang.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 常量配置
 * @author: KevinOy
 * @date: 2019/03/18 22:46
 */
public class Constants {
    public static Map<String, String> mysqlTypeMap = new HashMap<String, String>();
    static {//MySQL -> Java类型对照表，暂时只针对常用的几个做映射
        mysqlTypeMap.put("CHAR", "String");
        mysqlTypeMap.put("VARCHAR", "String");
        mysqlTypeMap.put("INTEGER", "Integer");
        mysqlTypeMap.put("TINYINT", "Integer");
        mysqlTypeMap.put("SMALLINT", "Integer");
        mysqlTypeMap.put("MEDIUMINT", "Integer");
        mysqlTypeMap.put("BIGINT", "Long");
        mysqlTypeMap.put("FLOAT", "Float");
        mysqlTypeMap.put("DOUBLE", "Double");
        mysqlTypeMap.put("DECIMAL", "BigDecimal");
        mysqlTypeMap.put("DATE", "Date");
        mysqlTypeMap.put("DATETIME", "Date");
        mysqlTypeMap.put("DATE", "Date");
    }

    public static final String SRC_MAIN_JAVA = "src/main/java";

    public static final String SRC_MAIN_RESOURCES = "src/main/resources";
}
