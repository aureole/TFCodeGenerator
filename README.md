# TF Code Generator

## 介绍
途方代码生成器，由于自己做项目时涉及到Mybatis Plus，而使用MyBatis Generator需要在IDEA和Eclipse中安装对应插件，个人感觉步骤略为繁琐，并且代码不能够实现自己定制化，所以开发了自用的代码生成器，**TF Code Generator采用Beetl模板引擎，根据数据库表字段及注释自动生成Entity, Mapper接口，MapperXML代码，帮助开发者简化重复代码，提高开发效率，同时支持自定义模板，实现自己的定制化输出需求。**

## 主要依赖
1. [Beetl  2.9.3 --超快的java模板引擎](http://ibeetl.com/)
2. [Hutool 4.5.1 --强大的Java基础工具类](https://hutool.cn/)
3. mysql-connector-java(目前仅支持MySQL)


## 初步体验

1. gie clone 本项目，建议使用IDEA打开本项目。

2. 建表，在MySQL(目前仅支持MySQL) 中创建示例表，表中字段*均需加上注释*。


2. 如下，修改src/main/java下TFCodeGeneratorTest.java类，修改相应信息，运行main方法，生成完成后，会在控制台显示生成位置，windows系统下会调用资源管理器打开生成目录，目录中为已生成好的entity, mapper, mapperXML文件，直接拷入web项目中即可使用。
```java
GenConfig config = new GenConfig();
config.setTemplatePath("/TFCodeTemplate");//模板路径
config.setPackageName("com.tufang.modules.sys.user");//包名
config.setModuleName("sys.user");//模块名，用于生成mapper xml文件分包
config.setOutputPath("D:\\CodeTemplates");//输出目录
config.setTablePrefix("tb");//表名前缀
config.setAuthor("KevinOy");//作者
config.setDbType("mysql");//数据库类型，暂时只支持MySQL
config.setUrl("jdbc:mysql://localhost:3306/tufang?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=GMT%2B8");
config.setUsername("root");
config.setPassword("123456");
config.setTableName("tb_sys_user");//生成表名，多个以,分隔
CodeGenerator c = CodeGenerator.GetInstance(config);
c.generate();
```

## Entity文件
![](https://gitee.com/itoysk/KevinImg/raw/master/tufang/code-generator/entity.png)

## Mapper接口
![](https://gitee.com/itoysk/KevinImg/raw/master/tufang/code-generator/mapper.png)

## MapperXML
![](https://gitee.com/itoysk/KevinImg/raw/master/tufang/code-generator/mapperxml.png)